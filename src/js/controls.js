module.exports = [{
    key: '+ *',
    text: 'Zoom in on the currently selected map'
},{
    key: '- *',
    text: 'Zoom out on the currently selected map'
}, {
    key: 'SHIFT',
    text: 'Hold down to allow quick placement of walls'
}, {
    key: 'CONTROL',
    text: 'Hold down to allow movement of placed wall points<br>Points will change for all walls that share it'
}, {
    key: 'A',
    text: 'Place light'
}, {
    key: 'E',
    text: 'Enable map lighting'
}, {
    key: 'D',
    text: 'Disable map lighting'
}, {
    key: 'O',
    text: 'Toggle closest door to mouse'
}, {
    key: 'T',
    text: 'Switch closest door to wall or vice versa'
}, {
    key: 'DELETE',
    text: 'Remove light|wall|door closest to mouse'
}, {
    key: 'W',
    text: 'Toggle walls on main window when lighting is on'
}, {
    key: 'S *',
    text: 'Put map on display window<br>This will also create the window if it doesn\'t exist'
}, {
    key: '[ *',
    text: 'Increase dimmer overlay on display window'
}, {
    key: '] *',
    text: 'Decrease dimmer overlay on display window'
}, {
    key: 'LEFT *',
    text: 'Scroll the map to the left'
}, {
    key: 'RIGHT *',
    text: 'Scroll the map to the right'
}, {
    key: 'TOP *',
    text: 'Scroll the map to the top'
}, {
    key: 'BOTTOM *',
    text: 'Scroll the map to the bottom'
}, {
    key: '',
    text: '*Holding ALT will cause this action on the display window'
}];
